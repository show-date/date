package com.mine.date.service;

import com.mine.date.model.DateModel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "rest/time")
public interface DateService {

    @RequestMapping(path = "/hour", method = RequestMethod.GET)
    String getHour();

    @RequestMapping(path = "/date", method = RequestMethod.GET)
    String getDate();

    @RequestMapping(path = "/date-hour", method = RequestMethod.GET)
    DateModel getDateHour();
}
