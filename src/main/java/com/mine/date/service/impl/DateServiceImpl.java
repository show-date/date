package com.mine.date.service.impl;

import com.mine.date.manager.DateManager;
import com.mine.date.model.DateModel;
import com.mine.date.service.DateService;
import org.springframework.stereotype.Service;

@Service
public class DateServiceImpl implements DateService {

    private final DateManager manager;

    public DateServiceImpl(DateManager manager) {
        this.manager = manager;
    }

    @Override
    public String getHour() {
        return manager.getHour();
    }

    @Override
    public String getDate() {
        return manager.getDate();
    }

    @Override
    public DateModel getDateHour() {
        return manager.geDateHour();
    }
}
