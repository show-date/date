package com.mine.date.manager;

import com.mine.date.model.DateModel;
import org.springframework.stereotype.Component;

import java.util.Calendar;

@Component("dateManager")
public class DateManager {


    public String getHour() {
        Calendar calendar = Calendar.getInstance();
        String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        String minutes = String.valueOf(calendar.get(Calendar.MINUTE));
        String seconds = String.valueOf(calendar.get(Calendar.SECOND));

        return hour + ":" + minutes + ":" + seconds;
    }

    public String getDate() {
        Calendar calendar = Calendar.getInstance();
        String dayOfMonth = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String month = String.valueOf(calendar.get(Calendar.MONTH));
        String year = String.valueOf(calendar.get(Calendar.YEAR));

        return month + "/" + dayOfMonth + "/" + year;
    }

    public DateModel geDateHour() {
        Calendar calendar = Calendar.getInstance();

        String dayOfMonth = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String month = String.valueOf(calendar.get(Calendar.MONTH));
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        String minutes = String.valueOf(calendar.get(Calendar.MINUTE));
        String seconds = String.valueOf(calendar.get(Calendar.SECOND));

        String date = month + "/" + dayOfMonth + "/" + year;
        String time = hour + ":" + minutes + ":" + seconds;

        return DateModel.builder().date(date).hour(time).build();
    }
}
